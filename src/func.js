const getSum = (str1, str2) => {
  let result = [];
  if(typeof str1 != 'string' || typeof str2 != 'string') {
    return false;
  }
  if((/^\d+$/.test(str1) || str1.length == 0) && (/^\d+$/.test(str2)  || str1.length == 0)) {
    let s1 = str1.split('').map(function(elem) {
      return parseInt(elem, 10);
    });
    let s2 = str2.split('').map(function(elem) {
      return parseInt(elem, 10);
    });
    for(let i = 0; i < Math.max(s1.length, s2.length); ++i) {
      result.push((s1[i] || 0) + (s2[i] || 0));
    }
    return result.join('');
  }
  else {
    return false;
  }
  
};

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  let str;
  let posts = 0;
  let comm = 0;
  for(let post of listOfPosts) {
    if(post.author == authorName) {
      ++posts;
    }
    if(post.hasOwnProperty("comments")) {
      for(let comment of post.comments) {
        if(comment.author == authorName) {
          ++comm;
        }
      }
    }
  }
  str = "Post:" + posts + ',comments:' + comm;
  return str;
};

const tickets=(people)=> {
  let cash = 0;
  for(let human of people) {
      if(cash >= human - 25) {
        cash += 25; 
      }
      else {
        return "NO";
      }
  }
  return "YES";
  
};


module.exports = {getSum, getQuantityPostsByAuthor, tickets};
//npm run test:junit